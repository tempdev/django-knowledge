from django.conf.urls import patterns, url
from knowledge.views import *

urlpatterns = patterns('knowledge.views',
    url(r'^$', 'knowledge_index', name='knowledge_index'),

    url(r'^questions/$', 'knowledge_list', name='knowledge_list'),

    url(r'^questions/(?P<question_id>\d+)/$',
        'knowledge_thread', name='knowledge_thread_no_slug'),

    url(r'^questions/(?P<category_slug>[a-z0-9-_]+)/$', 'knowledge_list',
        name='knowledge_list_category'),

    url(r'^questions/(?P<question_id>\d+)/(?P<slug>[a-z0-9-_]+)/$',
        'knowledge_thread', name='knowledge_thread'),

    url(r'^questions/edit/(?P<question_id>\d+)/$',
        'knowledge_edit', name='knowledge_edit'),

    url(r'^moderate/(?P<model>[a-z]+)/'
        r'(?P<lookup_id>\d+)/(?P<mod>[a-zA-Z0-9_]+)/$',
        'knowledge_moderate', name='knowledge_moderate'),

    url(r'^ask/$', 'knowledge_ask', name='knowledge_ask'),

    url(r'categories/$', CategoryList.as_view(), name="category_list"),
    url(r'categories/add/$', CategoryCreate.as_view(), name='category_add'),
    url(r'categories/(?P<pk>\d+)/$', CategoryUpdate.as_view(), name='category_update'),
    url(r'categories/(?P<pk>\d+)/delete/$', CategoryDelete.as_view(), name='category_delete'),

)
